# launch_ws

#### 介绍
练习launch的工作空间，包含相关例子

#### 软件架构
软件架构说明


#### 安装教程

```
cd ~
git clone https://gitee.com/ncnynl/launch_ws
cd launch_ws
colcon build --symlink-install 

```

#### 使用说明

参考： https://www.ncnynl.com/archives/202202/5063.html

目录:

 - ROS2与launch入门教程-创建ROS2的launch(启动)文件
 - ROS2与launch入门教程-使用Launch启动和监控多个节点
 - ROS2与launch入门教程-使用substitutions
 - ROS2与launch入门教程-launch文件中使用事件处理程序
 - ROS2与launch入门教程-使用ROS2 launch文件启动大型项目
 - ROS2与launch入门教程-使用Python、XML 和 YAML格式编写ROS2 launch文件
 - ROS2与launch入门教程-如何迁移ROS1的launch文件到ROS2


