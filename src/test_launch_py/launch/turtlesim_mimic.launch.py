#引用launch相关包
from launch import LaunchDescription
from launch_ros.actions import Node

#定义launch描述
def generate_launch_description():
    return LaunchDescription([
        #调用节点turtlesim_node,生成第一个小海龟
        Node(
            package='turtlesim',
            namespace='turtlesim1',
            executable='turtlesim_node',
            name='sim'
        ),
        #调用节点turtlesim_node,生成第二个小海龟
        Node(
            package='turtlesim',
            namespace='turtlesim2',
            executable='turtlesim_node',
            name='sim'
        ),
        #调用节点mimic,实现跟随
        Node(
            package='turtlesim',
            executable='mimic',
            name='mimic',
            remappings=[
                ('/input/pose', '/turtlesim1/turtle1/pose'),
                ('/output/cmd_vel', '/turtlesim2/turtle1/cmd_vel'),
            ]
        )
    ])